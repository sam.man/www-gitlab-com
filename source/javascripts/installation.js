var getUrlParameter = function getUrlParameter(param) {
  var pageURL = decodeURIComponent(window.location.search.substring(1)),
      urlParams = pageURL.split('&');

  for (var i = 0; i < urlParams.length; i++) {
    var parameterName = urlParams[i].split('=');

    if (parameterName[0] === param) {
      return parameterName[1] === undefined ? true : parameterName[1];
    }
  }
};

$(function() {
  var internalNavigationEvent = 'onpopstate' in window ? 'popstate' : 'hashchange';
  var hasVersionParam = (new RegExp('version=ce')).test(window.location.href);
  var scrollToDistro = function scrollToDistro(el) {
    $(window).scrollTop(el.offset().top - 230);
  };
  var showDistro = function showDistroy(el) {
    el.removeClass('hidden')
      .addClass('is-active')
      .prev()
      .addClass('is-active');

    setTimeout(function() {
      scrollToDistro(el);
    });
  };

  if (location.hash) {
    showDistro($(location.hash.split('?')[0]));
  }

  $('.js-edition-picker').toggleClass('hidden', !hasVersionParam);
  $('.js-platform-ce').toggleClass('hidden', !hasVersionParam);
  $('.js-default-ee-notice').toggleClass('hidden', hasVersionParam);
  $('.js-platform-ee').toggleClass('hidden', hasVersionParam);
  $('.js-distro-tile-ce-only').next('.js-distro-content').find('.js-platform-ce').removeClass('hidden');
  
  var $allDistros = $('.js-distro-content');
  $('.js-distro-tile').on('click', function(e) {
    var isOpen = this.parentNode.classList.contains('is-active');
    $allDistros.addClass('hidden');
    $('.distro-tile.is-active').removeClass('is-active');

    if (!isOpen) {
      showDistro($(this.getAttribute('href')));
    } else {
      e.preventDefault();
      location.hash = '';
      return false;
    }
  });

  $('.js-toggle-instructions').on('click', (e) => {
    var $this = $(e.target);
    var $distro = $this.closest('.js-distro-content');
    e.preventDefault();

    $('.js-install-instructions', $distro).addClass('hidden');
    $('.js-platform-' + $this.attr('data-toggle-type'), $distro).removeClass('hidden');
    scrollToDistro($distro);
  });

  if (getUrlParameter('default') === 'ee') {
    $('.js-install-instructions').addClass('hidden');
    $('.js-platform-ee').removeClass('hidden');
  }

  window.initCopyButton('.js-copy-btn');
});

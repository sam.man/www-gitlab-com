---
layout: markdown_page
title: "Edge Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

- [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce);
  please use confidential issues for topics that should only be visible to team members at GitLab.
- Chat channels; please use chat channels for questions that don't seem appropriate to use the issue tracker for.
  - [#triage](https://gitlab.slack.com/messages/triage): general triage team channel.
  - [#mr-coaching](https://gitlab.slack.com/archives/mr-coaching): for general
    conversation about Merge Request coaching.
  - [#opensource](https://gitlab.slack.com/archives/opensource): for general
    conversation about Open Source.

## Overview

The Edge Team is part of engineering responsible for improving GitLab
in multiple ways:

* Improving GitLab code/workflows in ways that engineering groups may not be doing:
  * Increasing contributors productivity by improving the development setup,
    workflow, processes, and tools
  * Reducing the duration of the GitLab test suite, and improving its stability
    by eliminating transient failures
  * Identifying architectural and [backstage](/jobs/specialist/backstage/)
    improvements as well as technical debt
* Interfacing with the community
  * [Triaging issues](/jobs/specialist/issue-triage/) reported by the community
  * [Reviewing and accepting merge requests](/jobs/merge-request-coach/) submitted by the community
* Advancing GitLab's contributions to other related open source projects
  * For example: Git command-line
* Evaluating code quality in potential partnerships

## Specialities

* [Issue Triage](/handbook/quality/edge/issue-triage/)
